# GoCD

Continuous Integration & Continuous Delivery

#HSLIDE

@snap[west span-30]

#GoCD Architecture 

@snapend

@snap[east span-70]

![Explained](images/GoCD-Architecture__1_.png)

@snapend

#HSLIDE


@snap[top span-90]

![Key Terms](images/GoCD-Terminology.png)

@snapend

#HSLIDE

@snap[top span-90]

![Key Terms](images/CICD-Workflow__1_.png)

@snapend

#HSLIDE

@snap[top-left]

![First Pipeline](images/Pipeline-Build-1.png)

@snapend

#HSLIDE

@snap[top-right]

![Second Pipeline](images/Pipeline-Test-2.png)

@snapend

#HSLIDE

@snap[top]

![Prod Pipeline Build](images/Pipeline-Prod-3.png)

@snapend